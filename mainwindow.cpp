#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "aboutdialog.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    absorbdamage helper();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_simulateButton_clicked()
{
    int iDamage = ui->damageBox->value();

    double iReduced14 = helper.AbsorbDamage14(iDamage);
    bool bAF14 = helper.bAFWorked;
    int iRounded14 = helper.RoundAbsorb(iReduced14);

    double iReduced15 = helper.AbsorbDamage15(iDamage);
    bool bAF15 = helper.bAFWorked;
    int iRounded15 = helper.RoundAbsorb(iReduced15);

    QString out = QString("BETA 14: %1 / AF: %2 / ROUNDED: %3\n"
                          "BETA 15: %4 / AF: %5 / ROUNDED: %6\n").arg(QString::number(iReduced14),
                                                                      QString::number(bAF14),
                                                                      QString::number(iRounded14),
                                                                      QString::number(iReduced15),
                                                                      QString::number(bAF15),
                                                                      QString::number(iRounded15)
                                                                      );
    ui->output->setPlainText(out);
}


//loadsof setting code below

void MainWindow::on_AFCheckbox_toggled(bool checked)
{
    helper.bHasAF = checked;
}

void MainWindow::on_DCCheckbox_toggled(bool checked)
{
    helper.bHasDC = checked;
}

void MainWindow::on_SAACheckbox_toggled(bool checked)
{
    helper.bHasSAA = checked;
}

void MainWindow::on_MectoidBox_toggled(bool checked)
{
    helper.bHasMectoidShield = checked;
}

void MainWindow::on_BerserkerDrugsBox_toggled(bool checked)
{
    helper.bIsUsingBerserkerDrugs = checked;
}

void MainWindow::on_OFABox_toggled(bool checked)
{
    helper.bIsUsingOFA = checked;
}

void MainWindow::on_AcidBox_toggled(bool checked)
{
    helper.bIsOnAcid = checked;
}

void MainWindow::on_MergeWill_valueChanged(int arg1)
{
    helper.iMindMergeWill = arg1;
}

void MainWindow::on_DRValue_valueChanged(double arg1)
{
    helper.dSuitDR = arg1 * (double) 10;
}

void MainWindow::on_MeleeBox_toggled(bool checked)
{
    helper.bIsMelee = checked;
}

void MainWindow::on_KSMBox_toggled(bool checked)
{
    helper.bIsKSM = checked;
}

void MainWindow::on_ShotgunBox_toggled(bool checked)
{
    helper.bIsShotgun = checked;
}

void MainWindow::on_APBox_toggled(bool checked)
{
    helper.bHasAPAmmo = checked;
}

void MainWindow::on_GaussBox_toggled(bool checked)
{
    helper.bIsGauss = checked;
}

void MainWindow::on_ImprovedGaussBox_toggled(bool checked)
{
    helper.bIsImprovedGauss = checked;
}

void MainWindow::on_CombinedArmsBox_toggled(bool checked)
{
    helper.bHasCombinedArms = checked;
}

void MainWindow::on_AboutButton_clicked()
{
    AboutDialog about(this);
    about.exec();
}

void MainWindow::on_coverBox_currentIndexChanged(int index)
{
    switch(index)
    {
    case 0:
        helper.eCoverType = helper.ECOVER_NONE;
        break;
    case 1:
        helper.eCoverType = helper.ECOVER_FULL;
        break;
    case 2:
        helper.eCoverType = helper.ECOVER_LOW;
        break;
    }

}

void MainWindow::on_hunkerBox_toggled(bool checked)
{
    helper.bIsHunkered = checked;
}
void MainWindow::on_WTSBox_toggled(bool checked)
{
    helper.bHasWTS = checked;
}

void MainWindow::on_OTSDRBox_toggled(bool checked)
{
    helper.bOTSDR = checked;
}

void MainWindow::on_ChitinBox_toggled(bool checked)
{
    helper.bHasChitin = checked;
}
