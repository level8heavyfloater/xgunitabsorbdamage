#-------------------------------------------------
#
# Project created by QtCreator 2015-03-14T18:40:42
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = XGUnitAbsorbDamage
TEMPLATE = app

RC_ICONS = MEC_ABSORTION_FIELDS.ico

SOURCES += main.cpp\
        mainwindow.cpp \
    absorbdamage.cpp \
    aboutdialog.cpp

HEADERS  += mainwindow.h \
    absorbdamage.h \
    aboutdialog.h

FORMS    += mainwindow.ui \
    aboutdialog.ui
