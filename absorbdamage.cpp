#include "absorbdamage.h"

using namespace std;

// this used to be part of a smaller c program
// that's why it looks awful and uses C functions

absorbdamage::absorbdamage():
    bHasAF(false),
    bHasDC(false),
    bHasSAA(false),
    iMindMergeWill(false),
    bHasMectoidShield(false),
    bIsUsingBerserkerDrugs(false),
    bIsUsingOFA(false),
    dSuitDR(0.0),
    bIsOnAcid(false),
    bIsHunkered(false),
    bHasWTS(false),
    bOTSDR(false),
    bHasChitin(false),
    eCoverType(ECOVER_NONE),
    bIsMelee(false),
    bIsKSM(false),
    bIsShotgun(false),
    bHasAPAmmo(false),
    bIsGauss(false),
    bHasCombinedArms(false),
    bIsImprovedGauss(false),
    bAFWorked(false)
{
    srand(time(0));
}

double absorbdamage::frand()
{
    double fRetVal = (double) rand() / (double) RAND_MAX / 1.0;
    return fRetVal;
}

double absorbdamage::AbsorbDamage14(int IncomingDamage)
{
    double fReturnDmg;
//    int iReturnDmg;

    fReturnDmg = (double) IncomingDamage;
    bAFWorked = false;

    if(bHasMectoidShield)
        fReturnDmg *= 0.50;

    if(bHasSAA)
        fReturnDmg *= 0.67;

    if(bHasAF)
    {
        if(fReturnDmg > (double) 1)
        {
            fReturnDmg *= (0.60 + (0.40 * fmin(2.0 / fReturnDmg, 1.0)));
            bAFWorked = true;
        }
    }


    if(fmod(dSuitDR, 100.0) > 0.0)
    {
        fReturnDmg = fReturnDmg - fmod(dSuitDR, 100.0)  / (double) 10;
    }


    if(iMindMergeWill > 0 && !bHasMectoidShield)
    {
        fReturnDmg -= ( (double) iMindMergeWill / (double) 45 );
    }

    if(bHasDC)
        fReturnDmg -= 1.50;

    if(bIsUsingBerserkerDrugs)
        fReturnDmg *= 0.60;

    /*  melee weapon damage reduction?
     *  if((kWeapon.IsMelee() || kWeapon.IsA('XGWeapon_MEC_KineticStrike')) && (((GetInventory()) != none) && GetInventory().GetRearBackpackItem(72) != none) || GetCharacter().m_kChar.iType == 13)
        {
            fReturnDmg *= 0.60;
        }
     */

    if(bIsMelee || bIsKSM)
    {
        if(bHasChitin)
        {
            fReturnDmg *= 0.60;
        }
    }

    if(bIsUsingOFA)
        fReturnDmg -= 1.0;

    if(bIsOnAcid)
    {
        fReturnDmg += fmax( (double) 3, 0.50 * ( (double) IncomingDamage - fReturnDmg ) );
    }

    /* cover stuff would be here, I might implement it later */

    if(eCoverType != ECOVER_NONE)
    {
        double fDist = fReturnDmg;

        if(eCoverType == ECOVER_LOW)
        {
            fReturnDmg -= 0.6670;
        }
        else
        {
            fReturnDmg -= 1.0;
        }

        if(bIsHunkered)
        {
            if(eCoverType == ECOVER_LOW)
            {
                fReturnDmg -= 0.6670;
            }
            else
            {
                fReturnDmg -= 1.0;
            }
        }
        if(bHasWTS)
        {
            fReturnDmg -= 1.50;
        }
        if(bOTSDR)
        {
            fReturnDmg -= 0.50;
        }
        if(bHasCombinedArms)
        {
            fReturnDmg += 1.0;
        }
        fReturnDmg = fmin(fReturnDmg, fDist);
    }


    if(bHasAPAmmo)
        fReturnDmg += 1.3333330;

    if(bIsGauss)
        fReturnDmg += 0.33; /* Beta 14i Gauss DR penetration
                              TODO: Maybe unhardcode this? */

    fReturnDmg = fmin(fReturnDmg, (double) IncomingDamage);

    /* Uber Ethereal DR code would be here */

    if(fReturnDmg < (double) IncomingDamage)
    {
        if(bIsShotgun)
            fReturnDmg -= (0.50 * ( (double) IncomingDamage - fReturnDmg ));

        fReturnDmg = fmax(fReturnDmg, (double) 0);
        return fReturnDmg;
    }
    return IncomingDamage;
}

double absorbdamage::AbsorbDamage15(int IncomingDamage)
{
    double fReturnDmg;
//    int iReturnDmg;

    fReturnDmg = (double) IncomingDamage;
    bAFWorked = false;

    /* m_aCurrentStats is innate suit DR */

    if(fmod(dSuitDR, 100.0) > 0.0)
    {
        fReturnDmg = fReturnDmg - fmod(dSuitDR, 100.0)  / (double) 10;
    }

    if(iMindMergeWill > 0 && !bHasMectoidShield)
    {
        fReturnDmg -= ( (double) iMindMergeWill / (double) 50 );
    }

    if(bHasDC)
        fReturnDmg -= 1.50;

    if(bIsUsingOFA)
        fReturnDmg -= 1.0;

    /* Uber Ethereal DR code would be here */

    if(bIsUsingBerserkerDrugs)
        fReturnDmg *= 0.60;

    /*  melee weapon damage reduction?
     *  if((kWeapon.IsMelee() || kWeapon.IsA('XGWeapon_MEC_KineticStrike')) && (((GetInventory()) != none) && GetInventory().GetRearBackpackItem(72) != none) || GetCharacter().m_kChar.iType == 13)
        {
            fReturnDmg *= 0.60;
        }
     */

    if(bIsMelee || bIsKSM)
    {
        if(bHasChitin)
        {
            fReturnDmg *= 0.60;
        }
    }

    if(bHasMectoidShield)
        fReturnDmg *= 0.50;

    if(bHasSAA)
        fReturnDmg *= 0.67;

    if(bHasAF)
    {
        if(fReturnDmg > (double) 1)
        {
            fReturnDmg *= (0.60 + (0.40 * fmin(2.0 / fReturnDmg, 1.0)));
            bAFWorked = true;
        }
    }

    if(bIsOnAcid)
    {
        fReturnDmg += fmax( (double) 3, 0.50 * ( (double) IncomingDamage - fReturnDmg ) );
    }

    /* cover stuff would be here, implemented 2015/03/15 */

   if(eCoverType != ECOVER_NONE)
   {
       if(eCoverType == ECOVER_LOW)
       {
           fReturnDmg -= 0.6670;
       }
       else
       {
           fReturnDmg -= 1.0;
       }

       if(bIsHunkered)
       {
           if(eCoverType == ECOVER_LOW)
           {
               fReturnDmg -= 0.6670;
           }
           else
           {
               fReturnDmg -= 1.0;
           }
       }
       if(bHasWTS)
       {
           fReturnDmg -= 1.50;
       }
       if(bOTSDR)
       {
           if(eCoverType == ECOVER_LOW)
           {
               fReturnDmg -= 0.6670;
           }
           else
           {
               fReturnDmg -= 1.0;
           }
       }
   }

    if(bHasCombinedArms)
        fReturnDmg += 1.0;

    if(bHasAPAmmo)
        fReturnDmg += 2.0;

    if(bIsGauss)
    {
        if(bIsImprovedGauss)
        {
            fReturnDmg += (double) 3 * 0.34;
        }
        else
        {
            fReturnDmg += 0.34; /* Beta 15 Gauss DR penetration
                                   TODO: Maybe unhardcode this? */
        }
    }
    fReturnDmg = fmin(fReturnDmg, (double) IncomingDamage);

    if(fReturnDmg < (double) IncomingDamage)
    {
        if(bIsShotgun)
            fReturnDmg -= (0.50 * ( (double) IncomingDamage - fReturnDmg ));

    }
    fReturnDmg = fmax(fReturnDmg, (double) 0);

    return fReturnDmg;
    return IncomingDamage; /* intentionally unreachable for parity with the decompiled code.
                              this will be fixed with b15c. */
}

int absorbdamage::RoundAbsorb(double fReturnDmg)
{
    int iReturnDmg = 0;

    if(bIsKSM)
        iReturnDmg = (int) floor(fReturnDmg);
    else
    {
        if(frand() < (fReturnDmg - floor(fReturnDmg)))
        {
            iReturnDmg = (int) ceil(fReturnDmg);
        }
        else
        {
            iReturnDmg = (int) floor(fReturnDmg);
        }
    }

    return iReturnDmg;
}
