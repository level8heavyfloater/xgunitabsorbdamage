#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "absorbdamage.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_AFCheckbox_toggled(bool checked);

    void on_DCCheckbox_toggled(bool checked);

    void on_SAACheckbox_toggled(bool checked);

    void on_MectoidBox_toggled(bool checked);

    void on_BerserkerDrugsBox_toggled(bool checked);

    void on_OFABox_toggled(bool checked);

    void on_AcidBox_toggled(bool checked);

    void on_MergeWill_valueChanged(int arg1);

    void on_DRValue_valueChanged(double arg1);

    void on_MeleeBox_toggled(bool checked);

    void on_KSMBox_toggled(bool checked);

    void on_ShotgunBox_toggled(bool checked);

    void on_APBox_toggled(bool checked);

    void on_GaussBox_toggled(bool checked);

    void on_ImprovedGaussBox_toggled(bool checked);

    void on_CombinedArmsBox_toggled(bool checked);

    void on_simulateButton_clicked();

    void on_AboutButton_clicked();

    void on_coverBox_currentIndexChanged(int index);

    void on_hunkerBox_toggled(bool checked);

    void on_WTSBox_toggled(bool checked);

    void on_OTSDRBox_toggled(bool checked);

    void on_ChitinBox_toggled(bool checked);

private:
    Ui::MainWindow *ui;
    absorbdamage helper;
};

#endif // MAINWINDOW_H
