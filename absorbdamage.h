#ifndef ABSORBDAMAGE_H
#define ABSORBDAMAGE_H

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <ctime>

class absorbdamage
{
public:
    absorbdamage();
    enum ECoverType
    {
        ECOVER_NONE,
        ECOVER_LOW,
        ECOVER_FULL
    };

    //DEFENDER PERKS
    bool bHasAF;
    bool bHasDC;
    bool bHasSAA;
    int iMindMergeWill;
    bool bHasMectoidShield;
    bool bIsUsingBerserkerDrugs;
    bool bIsUsingOFA;
    double dSuitDR;
    bool bIsOnAcid;
    bool bIsHunkered;
    bool bHasWTS;
    bool bOTSDR;
    bool bHasChitin;
    ECoverType eCoverType;

    // ATTACKER PERKS
    bool bIsMelee;
    bool bIsKSM;
    bool bIsShotgun;
    bool bHasAPAmmo;
    bool bIsGauss;
    bool bHasCombinedArms;
    bool bIsImprovedGauss;

    bool bAFWorked;
    double AbsorbDamage14(int IncomingDamage);
    double AbsorbDamage15(int IncomingDamage);

    int RoundAbsorb(double fReturnDmg);

private:
    double frand();
};

#endif // ABSORBDAMAGE_H
